import turtle


def draw_triangle(points, color, my_turtle):
    my_turtle.fillcolor(color)
    my_turtle.up()
    my_turtle.goto(points[0][0], points[0][1])
    my_turtle.down()
    my_turtle.begin_fill()
    my_turtle.goto(points[1][0], points[1][1])
    my_turtle.goto(points[2][0], points[2][1])
    my_turtle.goto(points[0][0], points[0][1])
    my_turtle.end_fill()


def get_middle(p1, p2):
    return (p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2


def sierpinski_triangle(points, degree, my_turtle):
    draw_triangle(points, 'pink', my_turtle)

    if degree > 0:
        sierpinski_triangle([points[0],
                             get_middle(points[0], points[1]), get_middle(points[0], points[2])],
                            degree - 1, my_turtle)
        sierpinski_triangle([points[1],
                             get_middle(points[0], points[1]), get_middle(points[1], points[2])],
                            degree - 1, my_turtle)
        sierpinski_triangle([points[2], get_middle(points[2], points[1]),
                             get_middle(points[0], points[2])], degree - 1, my_turtle)


def main():
    my_turtle = turtle.Turtle()

    # speed entering
    while True:
        try:
            speed = input('Enter speed: ')
            my_turtle.speed(int(speed))
            break
        except ValueError:
            print('''Oops... What`s wrong with this speed? 
             Try enter a number.''')
            continue

    # pen color entering
    while True:
        try:
            color = input('Enter pen color, you prefer to use: ')
            my_turtle.color(color)
            break
        except turtle.TurtleGraphicsError:
            print('''Oops... strange color
    try another one''')
            continue

    drawing_window = turtle.Screen()
    vertices = [[-200, -100], [0, 200], [200, -100]]
    sierpinski_triangle(vertices, 3, my_turtle)
    drawing_window.exitonclick()
    turtle.done()


if __name__ == "__main__":
    main()

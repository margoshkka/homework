from task2.math_matrix import transpose, merge,reverse
from task2.generator import generate_initial_field, generate_number


class GameLogic:
    def __init__(self, field_size):
        self.field_size = field_size
        self.field = [[0 for i in range(field_size)] for j in range(field_size)]
        self.score = 0
        self.field = generate_initial_field(self.field)
        self.has_moves = True

    def cover(self):
        new = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        done = False
        for i in range(4):
            count = 0
            for j in range(4):
                if self.field[i][j] != 0:
                    new[i][count] = self.field[i][j]
                    if j != count:
                        done = True
                    count += 1
        self.field = new
        return (done)

    def print_score(self):
        print('Your score: ' + self.score)

    def move_left(self):
        print("left")
        is_done = self.cover()
        temp = merge(self.field)
        self.field = temp[0]
        is_done = is_done or temp[1]
        self.cover()
        return (is_done)

    def move_right(self):
        print("right")
        self.field = reverse(self.field)
        is_done = self.cover()
        temp = merge(self.field)
        self.field = temp[0]
        is_done = is_done or temp[1]
        self.cover()
        self.field = reverse(self.field)
        return (is_done)

    def move_up(self):
        print("up")
        self.field = transpose(self.field)
        is_done = self.cover()
        temp = merge(self.field)
        self.field = temp[0]
        is_done = is_done or temp[1]
        self.cover()
        self.field = transpose(self.field)
        return (is_done)

    def move_down(self):
        print("down")
        self.field = reverse(transpose(self.field))
        is_done = self.cover()
        temp = merge(self.field)
        self.field = temp[0]
        is_done = is_done or temp[1]
        self.cover()
        self.field  = transpose(reverse(self.field))
        return is_done

    def check_has_moves(self):
        for i in range(self.field_size - 1):  # intentionally reduced to check the row on the right and below
            for j in range(self.field_size - 1):  # more elegant to use exceptions but most likely this will be their solution
                if self.field[i][j] == self.field[i + 1][j] or self.field[i][j + 1] == self.field[i][j]:
                    self.has_moves = True
                    return
        for i in range(self.field_size):  # check for any zero entries
            for j in range(self.field_size):
                if self.field[i][j] == 0:
                    self.has_moves = True
                    return
        for k in range(self.field_size - 1):  # to check the left/right entries on the last row
            if self.field[self.field_size - 1][k] == self.field[self.field_size - 1][k + 1]:
                self.has_moves = True
                return
        for j in range(self.field_size - 1):  # check up/down entries on last column
            if self.field[j][self.field_size - 1] == self.field[j + 1][self.field_size - 1]:
                self.has_moves = True
                return
        self.has_moves = False
        return

    def state(self):
        for i in range(self.field_size):
            for j in range(self.field_size):
                if self.field[i][j] == 2048:
                    return 'win'
        self.check_has_moves()
        if self.has_moves:
            return 'go ahead'
        return 'lose'

    def get_score(self):
        raise NotImplementedError

    def get_field(self):
        for line in self.field:
            for i in line:
                print(str(i), end=' ')
            print('')

    def generate_new(self):
        self.field = generate_number(self.field)
        return self.field

import numpy


def empty_values(field):
    empty = []
    i = 0
    j = 0
    for st in field:
        for val in st:
            if val == 0:
                empty.append([i,j])

            j += 1
        j = 0
        i += 1
    return empty


def generate_number(field):
    # some generation
    empty = empty_values(field)  # find all empty cells in the field

    place = numpy.random.choice(range(0, (len(empty))), None, False)
    value = numpy.random.choice([2, 4], None, False, p=[0.9, 0.1])

    field[empty[place][0]][empty[place][1]] = value

    return field


def generate_initial_field(field):
    print('\nStart the game\n\ninitial field\n')

    for i in range(3):
        field = generate_number(field)
    return field


def reverse(matrix):
    new = []
    for i in range(len(matrix)):
        new.append([])
        for j in range(len(matrix[0])):
            new[i].append(matrix[i][len(matrix[0]) - j - 1])
    return new


def transpose(matrix):
    new = []
    for i in range(len(matrix[0])):
        new.append([])
        for j in range(len(matrix)):
            new[i].append(matrix[j][i])
    return new


def merge(matrix):
    done = False
    for i in range(4):
        for j in range(3):
            if matrix[i][j] == matrix[i][j+1] and matrix[i][j] != 0:
                matrix[i][j] *= 2
                matrix[i][j + 1] = 0
                done = True

    return (matrix, done)
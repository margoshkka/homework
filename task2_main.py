from random import *
from tkinter import *
from task2.game_logic import GameLogic

# constants
SIZE = 900
GRID_LEN = 4
GRID_PADDING = 10


BACKGROUND_COLOR_GAME = "#EA94C8"
BACKGROUND_COLOR_CELL_EMPTY = "#FDFDFD"
BACKGROUND_COLOR_DICT = {2: "#F0F0F0", 4: "#E0DFDF", 8: "#C3C3C3", 16: "#A9A9A9", \
                         32: "#808080", 64: "#5F5F5F", 128: "#424242", 256: "#353434", \
                         512: "#272727", 1024: "#181818", 2048: "#000000"}
CELL_COLOR_DICT = {2: "#776e65", 4: "#776e65", 8: "#f9f6f2", 16: "#f9f6f2", \
                   32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", 256: "#f9f6f2", \
                   512: "#f9f6f2", 1024: "#f9f6f2", 2048: "#f9f6f2"}
FONT = ("Helvetica", 36, "bold")

KEY_UP = "'w'"
KEY_DOWN = "'s'"
KEY_LEFT = "'a'"
KEY_RIGHT = "'d'"


# def calculate_location():
#     ws = Tk.winfo_screenwidth()  # width of the screen
#     hs = Tk.winfo_screenheight()  # height of the screen
#     x = (ws / 2) - (SIZE / 2)
#     y = (hs / 2) - (SIZE / 2)
#
#     return Tk.geometry('%dx%d+%d+%d' % (SIZE, SIZE, x, y))
#

class VisualGameField(Frame):
    def __init__(self):
        Frame.__init__(self,)
        self.game = GameLogic(4)

        self.grid()
        self.master.title('2048')
        self.master.bind("<Key>", self.key_down)

        self.commands = {KEY_UP: self.game.move_up, KEY_DOWN: self.game.move_down, KEY_LEFT: self.game.move_left, KEY_RIGHT: self.game.move_right}

        self.visual_cells = []
        self.init_field()
        self.redraw_cells()

        self.mainloop()

    def init_field(self):
        background = Frame(self, bg=BACKGROUND_COLOR_GAME, width=SIZE, height=SIZE)
        background.grid()
        for i in range(GRID_LEN):
            grid_row = []
            for j in range(GRID_LEN):
                cell = Frame(background, bg=BACKGROUND_COLOR_CELL_EMPTY, width=SIZE / GRID_LEN, height=SIZE / GRID_LEN)
                cell.grid(row=i, column=j, padx=GRID_PADDING, pady=GRID_PADDING)
                # font = Font(size=FONT_SIZE, family=FONT_FAMILY, weight=FONT_WEIGHT)
                t = Label(master=cell, text="", bg=BACKGROUND_COLOR_CELL_EMPTY, justify=CENTER, font=FONT, width=4,
                          height=2)
                t.grid()
                grid_row.append(t)

            self.visual_cells.append(grid_row)

    def redraw_cells(self):
        for i in range(GRID_LEN):
            for j in range(GRID_LEN):
                new_number = self.game.field[i][j]
                if new_number == 0:
                    self.visual_cells[i][j].configure(text="", bg=BACKGROUND_COLOR_CELL_EMPTY)
                else:
                    self.visual_cells[i][j].configure(text=str(new_number), bg=BACKGROUND_COLOR_DICT[new_number],
                                                      fg=CELL_COLOR_DICT[new_number])
        self.update_idletasks()

    def key_down(self, event):
        key = repr(event.char)
        if key in self.commands:
            done = self.commands[repr(event.char)]()
            if done:
                temp = self.game.generate_new()
                self.game.field = temp
                self.redraw_cells()
                done = False
                if self.game.state() == 'win':
                    self.visual_cells[1][1].configure(text="You", bg=BACKGROUND_COLOR_CELL_EMPTY)
                    self.visual_cells[1][2].configure(text="Win!", bg=BACKGROUND_COLOR_CELL_EMPTY)
                if self.game.state() == 'lose':
                    self.visual_cells[1][1].configure(text="You", bg=BACKGROUND_COLOR_GAME)
                    self.visual_cells[1][2].configure(text="Lose!", bg=BACKGROUND_COLOR_GAME)


def main():
    game_field = VisualGameField()


if __name__ == '__main__':
    main()